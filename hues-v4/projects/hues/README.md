# @ngcore/hues

## Installation

To install this library, run:

```bash
$ npm install --save @ngcore/hues @ngcore/core 
```

## Consuming the Library

From your Angular `AppModule`:

```typescript
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

// Import the library
import { NgCoreHuesModule } from '@ngcore/hues';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,

    // Specify the library as an import
    NgCoreHuesModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

Once the library is imported, you can use its components, directives and pipes in your Angular application:

```xml
<!-- You can now use the library component in app.component.html -->
<h1>
  {{title}}
</h1>
<sampleComponent></sampleComponent>
```


### GlobalThemeHelper

In order to be able to use `GlobalThemeHelper`,
you will need to do the following:

#### Copy the `styles` folder to your app.

#### Add the folloiwn line in the header secion of the `index.html`:

    <link id="global-theme" rel="stylesheet" href="styles/empty-theme.css">


#### Modify the `app.module`'s constructor as follows:

    constructor( 
        @Inject(DOCUMENT) private document,
        private globalThemeHelper: GlobalThemeHelper
    ) {
        let theme = this.globalThemeHelper.getTheme();
        if (theme) {
            this.document.getElementById('global-theme').setAttribute('href', theme);
      }
    }



## Development

To generate all `*.js`, `*.d.ts` and `*.metadata.json` files:

```bash
$ npm run build
```

To lint all `*.ts` files:

```bash
$ npm run lint
```

## License

MIT © [Harry Y](mailto:sidewaybot@gmail.com)

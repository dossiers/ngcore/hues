import { Injectable } from '@angular/core';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { LocalStorageService } from '@ngcore/core';


/**
 * Service to store/retrieve angular-material theme information.
 */
@Injectable({
  providedIn : 'root'
})
export class GlobalThemeHelper {
  // // Singleton.
  // private static _Instance: (GlobalThemeHelper | null) = null;
  constructor(
    private localStorageService: LocalStorageService
  ) { }
  // public static getInstance(): GlobalThemeHelper {
  //   return this._Instance || (this._Instance = new GlobalThemeHelper());
  // }

  // temporary
  private static KEY_MATERIAL_THEME = "global-theme";


  // Heck. We are dynamically changing the theme at app startup time.
  // Or, read it from the user prefs.
  private static THEME_MAP: {[name: string]: boolean} = {
    "styles/deeppurple-amber.css": true,
    "styles/indigo-pink.css": true,
    "styles/pink-bluegrey.css": true,
    "styles/purple-green.css": true
  };
  private _themes: (string[] | null) = null;
  private get themes(): string[] {
    if(! this._themes) {
     this._themes = Object.keys(GlobalThemeHelper.THEME_MAP);
    }
    return this._themes;
  }
  // private isThemeValid(theme: string): boolean {
  //   let valid = false;
  //   // ...
  //   return valid;
  // }
  // Cache.
  private _theme: (string | null) = null;

  public getTheme(): string {
    if (!this._theme) {
      // Pick a random theme of the day.
      // TBD: Initially, read it from user settings.
      // let storedTheme: string;
      // // if(this.localStorageService.hasStorage) {
      //   storedTheme = this.localStorageService.getItem(GlobalThemeHelper.KEY_MATERIAL_THEME);
      // // }
      let storedTheme: string = this.getDefaultTheme();
      if (storedTheme && storedTheme in GlobalThemeHelper.THEME_MAP) {
        this._theme = storedTheme;
      } else {
        let idx = Math.floor(Math.random() * this.themes.length);
        this._theme = this.themes[idx];
      }
    }
    return this._theme;
  }
  public getDifferentTheme(): string {
    let currentTheme = this._theme;
    while (currentTheme === this._theme) {
      let idx = Math.floor(Math.random() * this.themes.length);
      this._theme = this.themes[idx];
    }
    return this._theme;
  }

  public hasDefaultTheme(): boolean {
    let storedTheme = this.getDefaultTheme();
    // return (storedTheme && storedTheme in this.THEME_MAP);
    return (!!storedTheme);
  }
  public getDefaultTheme(): (string | null) {
    let storedTheme: string;
    // if(this.localStorageService.hasStorage) {
      storedTheme = this.localStorageService.getItem(GlobalThemeHelper.KEY_MATERIAL_THEME);
    // }
    if (storedTheme && storedTheme in GlobalThemeHelper.THEME_MAP) {
      return storedTheme;
    } else {
      return null;
    }
  }
  // Save the current theme as the "default" theme.
  public saveDefaultTheme() {
    if (this._theme) {
      // if(this.localStorageService.hasStorage) {
        this.localStorageService.setItem(GlobalThemeHelper.KEY_MATERIAL_THEME, this._theme);
      // }
    } else {
      // ignore.
      // (or, remove the stored theme??)
    }
  }

  public removeDefaultTheme() {
    // if(this.localStorageService.hasStorage) {
      this.localStorageService.removeItem(GlobalThemeHelper.KEY_MATERIAL_THEME);
    // }
  }

}
